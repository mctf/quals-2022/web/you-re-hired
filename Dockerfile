FROM ctf2:latest
ARG CTF2_FLAG
CMD ["/bin/bash"]
ADD config_flag.py /root/
RUN python3 /root/config_flag.py $CTF2_FLAG;rm /root/config_flag.py
WORKDIR /home/user/server
