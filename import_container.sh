#!/bin/bash

docker image rm ctf2:latest
docker import ctf2_cont
ctf2_image_name=$(docker images | awk '{print $3}' | awk 'NR==2')
docker image tag $ctf2_image_name ctf2:latest
